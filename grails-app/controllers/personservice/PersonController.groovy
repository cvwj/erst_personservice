package personservice

import org.springframework.transaction.TransactionStatus
import personservice.domain.PasswordConfiguration
import personservice.service.gateway.CPRRegisterGateway
import personservice.service.gateway.CPRRegisterGatewayException

class PersonController {

    CPRRegisterGateway cprRegisterGateway

    def renewPassword() {

        // Default message if everything succeed
        String message = "Password successfully updated"
        PasswordConfiguration.withTransaction {
            TransactionStatus status ->
                // Always get the first PasswordConfiguration
                PasswordConfiguration pc = PasswordConfiguration.first()

                // If it does not exists, create it with the params. Expected keys are the names of hte fields in the PasswordConfiguration
                if (!pc) {
                    pc = new PasswordConfiguration(params)
                } else {
                    // Set the password in cleartext
                    pc.cleartextPassword = params.cleartextPassword
                }

                // tru to save - rollback on failure
                if (!pc.save()) {
                    message = "Password not updated. Problems with saving in database: ${pc.errors}"
                    log.error message
                    status.setRollbackOnly()
                } else {
                    // Try to update password at CPR - rollback on failure.
                    try {
                        cprRegisterGateway.skiftPassword(params.newPassword)
                    }
                    catch (CPRRegisterGatewayException cprException) {
                        message = "Password not updated. Could not update password at CPR: ${cprException.errorCode}"
                        log.error message
                        status.setRollbackOnly()
                    }
                }
        }

        render text: message

    }


    def initPasswordConfiguration(PasswordConfiguration passwordConfiguration)
    {
        def message
        def first = PasswordConfiguration.first()
        if (first)
        {
            message = "A PasswordConfiguration already exists. Please update that instead of initialising a new PasswordConfiguration"
        }
        else if (!passwordConfiguration.save())
        {
             message = "It was not possible to store the new password configuration. Errors were: $passwordConfiguration.errors"
        }
        else
        {
            message = "A new password configuration has been inserted successfully"
        }

        render text: message
    }
}
