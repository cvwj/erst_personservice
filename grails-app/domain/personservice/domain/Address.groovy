package personservice.domain

class Address {

    // CPR startdate for being the officiel address of the person
    Date startDate
    boolean addressProtected
    AddressType addressType

    static belongsTo = [person: Person]
    static constraints = {
    }

    enum AddressType {
        NORMAL, CONTACT, SUPPLEMENTARY, CUSTODY
    }

}
