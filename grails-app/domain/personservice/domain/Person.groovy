package personservice.domain

class Person {

    // Asumption: all persons from CPR Services has a CPR number
    String cpr

    // If the person does not have a danish CPR then set the birthdate
    String firstName
    String middleName
    String lastName
    Status status
    boolean addressProtected
    String citizenshipCountry

    static hasMany = [addresses: Address]

    static constraints = {
    }

    /**
     * The CPR defines the following status for a Person:
     *
     * 01	aktiv,  bopæl i dansk folkeregister
     * 03	aktiv, speciel vejkode (9900-9999) i dansk folkeregister
     * 05	aktiv,  bopæl i grønlandsk folkeregister
     * 07	aktiv,  speciel vejkode (9900 - 9999) i grønlandsk folkeregister
     * 20	inaktiv, uden bopæl i dansk/grønlandsk folkeregister men tildelt personnummer af skattehensyn (kommunekoderne 0010, 0011, 0012 og 0019)
     * 30	inaktiv, annulleret personnummer
     * 50	inaktiv, slettet personnummer ved dobbeltnummer
     * 60	inaktiv, ændret personnummer ved ændring af fødselsdato og køn
     * 70	inaktiv, forsvundet
     * 80	inaktiv, udrejst
     * 90	inaktiv, død
     *
     * They combine into the following codes:
     * 01, 03, 05, 07: NORMAL
     * 20, 30, 40, 50, 60: INACTIVE
     * 70: DISAPPEARED
     * 80: ABROAD
     * 90: DEAD
     *
     * The code for UNDER_CUSTODY is set if the person is marked as under custody in CPR.
     */
    enum Status {
        // Known person, danish or greenlandic address - no issues
        NORMAL,
        // Set to inactive in CPR for different reasons
        INACTIVE,
        // Person is dead
        DEAD,
        // Person is marked as disapppeared
        DISAPPEARED,
        // Person is marked as being abroad - should therefore have a forign address
        ABROAD,
        // Person is under custody (værgemål). There should be an address for the custodian
        UNDER_CUSTODY
    }
}
