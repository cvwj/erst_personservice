package personservice.domain

class AddressDomestic extends Address {

    String street
    String houseNumber
    String floor
    String doorNumber
    String alias
    String zipCode
    String city
    String country

    static constraints = {
    }
}
