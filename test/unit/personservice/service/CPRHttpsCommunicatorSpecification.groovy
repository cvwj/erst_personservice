package personservice.service

import grails.plugin.spock.UnitSpec
import personservice.service.gateway.CPRHttpsCommunicator
import personservice.service.gateway.CPRRegisterGateway
import personservice.service.gateway.CPRRegisterGatewayErrorCode
import personservice.service.gateway.CPRRegisterGatewayException

class CPRHttpsCommunicatorSpecification extends UnitSpec {

    CPRHttpsCommunicator cprCommunicator

    void 'test logon with no token'() {
        setup:
        prepareRegisterMetaClass()
        boolean connectCalled
        CPRHttpsCommunicator.metaClass.connectLogon = {
            connectCalled = true
        }
        CPRHttpsCommunicator.metaClass.connectService = {String cpr, String service -> }
        prepareKvitResponseCode(CPRHttpsCommunicator.KVIT_RETURN_CODE_OK)
        cprCommunicator = new CPRHttpsCommunicator()

        when:
        cprCommunicator.request('', '')

        then:
        connectCalled
    }

    void 'test logon with token'() {
        setup:
        boolean connectCalled
        CPRHttpsCommunicator.metaClass.connectLogon = {
            connectCalled = true
        }
        CPRHttpsCommunicator.metaClass.connectService = {String cpr, String service -> }
        prepareKvitResponseCode(CPRHttpsCommunicator.KVIT_RETURN_CODE_OK)
        cprCommunicator = new CPRHttpsCommunicator()
        cprCommunicator.token = 'token'

        when:
        cprCommunicator.request('', '')

        then:
        !connectCalled
    }

    void 'test CPR communication throwing CPRRegisterGatewayException with LOGON_FAILURE'() {
        setup:
        prepareRegisterMetaClass()
        CPRHttpsCommunicator.metaClass.connectLogon = { }
        cprCommunicator = new CPRHttpsCommunicator()
        cprCommunicator.responseXML = getLogonResponse(910)

        when:
        cprCommunicator.request('', '')

        then:
        CPRRegisterGatewayException ex = thrown()
        ex.errorCode == CPRRegisterGatewayErrorCode.LOGON_FAILURE
    }

    void 'test successfull CPR communication'() {
        setup:
        prepareRegisterMetaClass()
        CPRHttpsCommunicator.metaClass.connectLogon = { }
        CPRHttpsCommunicator.metaClass.connectService = {String cpr, String service -> }
        cprCommunicator = new CPRHttpsCommunicator()
        cprCommunicator.responseXML = getLogonResponse(900)

        when:
        cprCommunicator.request('', '')

        then:
        notThrown CPRRegisterGatewayException
    }

    void 'test service request with unknown token'() {
        setup:
        prepareRegisterMetaClass()
        prepareKvitResponseCode(CPRHttpsCommunicator.KVIT_RETURN_CODE_UNKNOWN_TOKEN)
        CPRHttpsCommunicator.metaClass.logon = { }
        CPRHttpsCommunicator.metaClass.connectService = {String cpr, String service -> }
        boolean logonAndConnectCalled = false
        CPRHttpsCommunicator.metaClass.reconnectService = {String cpr, String service ->
            logonAndConnectCalled = true
        }
        cprCommunicator = new CPRHttpsCommunicator()

        when:
        cprCommunicator.request('101010-1010', CPRRegisterGateway.NAVNE3)

        then:
        logonAndConnectCalled
    }

    private prepareRegisterMetaClass() {
        registerMetaClass CPRRegisterGateway
        registerMetaClass CPRHttpsCommunicator
    }

    private prepareKvitResponseCode(String returnCode) {
        CPRHttpsCommunicator.metaClass.readKvitResponseCode = {
            return returnCode
        }
    }

    /*
    private prepareBasic() {
        CPRRegisterGateway.metaClass.populatePersonFromName { Person person, String xml -> }
        CPRRegisterGateway.metaClass.populatePersonFromStam { Person person, String xml -> }
        CPRRegisterGateway.metaClass.populatePersonFromPnrvaerg { Person person, String xml -> }
    }
    */

    private String getLogonResponse(returnCode) {
        "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>\n" +
                "<root xmlns=\"http://www.cpr.dk\">\n" +
                "<Gctp v=\"1.0\">" +
                "<Sik>" +
                "<Kvit r=\"returKode\" v=\"" + returnCode + "\"/>\n" +
                "</Sik>" +
                "</Gctp>" +
                "</root>"
    }

}
