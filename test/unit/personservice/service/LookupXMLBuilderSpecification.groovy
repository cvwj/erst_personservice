package personservice.service

import grails.plugin.spock.UnitSpec
import personservice.service.gateway.CPRRegisterGateway
import personservice.service.gateway.builders.LogonXMLBuilder

class LookupXMLBuilderSpecification extends UnitSpec {

    void 'test logon XML request'() {
        setup:
        def xml
        LogonXMLBuilder builder = new LogonXMLBuilder('xxx', 'yyy')

        when:
        xml = new XmlSlurper().parseText(builder.buildXML())

        then:
        def gctp = xml.depthFirst().find {it.name() == 'Gctp'}
        gctp.name() == 'Gctp'
        gctp.@v == '1.0'
        gctp.Stik.@function == 'signon'
        gctp.Stik.@userid == 'xxx'
        gctp.Stik.@password == 'yyy'

    }

}
