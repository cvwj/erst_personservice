package personservice.service

import grails.plugin.spock.UnitSpec
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.ParserRegistry
import personservice.domain.Person
import personservice.service.gateway.CPRHttpsCommunicator
import personservice.service.gateway.CPRRegisterGateway
import personservice.service.gateway.CPRRegisterGatewayErrorCode
import personservice.service.gateway.CPRRegisterGatewayException
import personservice.service.gateway.data.Navne3Data

class CPRRegisterGatewaySpecification extends UnitSpec {

    CPRRegisterGateway gateway

    void 'test logon failed'() {
        // run this unit test against mock cpr register implemented with CprController
        // CprController must run as a web app
        setup:
        CPRHttpsCommunicator.metaClass.handleConnectService = {String cpr, String service -> ''}
        gateway = new CPRRegisterGateway()
        gateway.cprCommunicator = prepareCprHttpsCommunicator('logonFailed')

        when:
        gateway.lookupNavne3('101010-1010')

        then:
        CPRRegisterGatewayException ex = thrown()
        ex.errorCode == CPRRegisterGatewayErrorCode.LOGON_FAILURE
    }

    void 'test logon OK'() {
        // run this unit test against mock cpr register implemented with CprController
        // CprController must run as a web app
        setup:
        CPRHttpsCommunicator.metaClass.handleConnectService = {String cpr, String service -> ''}
        gateway = new CPRRegisterGateway()
        gateway.cprCommunicator = prepareCprHttpsCommunicator('logonOK')

        when:
        gateway.lookupNavne3('101010-1010')

        then:
        CPRRegisterGatewayException ex = thrown()
        ex.errorCode == CPRRegisterGatewayErrorCode.NO_XML_RESPONSE
    }

    private CPRHttpsCommunicator prepareCprHttpsCommunicator(uripath) {
        CPRHttpsCommunicator cprCommunicator = new CPRHttpsCommunicator()
        cprCommunicator.endpoint = 'http://localhost:8080/testapp/cpr'
        cprCommunicator.uripath = uripath
        cprCommunicator.username = 'xxx'
        cprCommunicator.password = 'yyy'
        return cprCommunicator
    }

    /*
    void 'test receive NAME3 service response'() {
        setup:
        prepareRegisterMetaClass()
        prepareLogonKvitResponseCode(CPRHttpsCommunicator.KVIT_RETURN_CODE_OK)

        CPRHttpsCommunicator.metaClass.request = {String cpr, String service ->
            if (CPRRegisterGateway.NAVNE3 == service) {
                getNAME3ServiceResponse()
            }
        }

        Navne3Data navne3Data
        gateway = new CPRRegisterGateway()
        gateway.cprCommunicator = new CPRHttpsCommunicator()

        when:
        navne3Data = gateway.lookupNavne3('101010-1010')

        then:
        navne3Data.fornavn == 'Katrine'
        navne3Data.mellemnavn == 'Gangelhoff Damgaard'
        navne3Data.efternavn == 'Knudsen'
    }
    */

    /*
    private String getNAME3ServiceResponse() {
        "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>" +
                "<root xmlns=\"http://www.cpr.dk\">" +
                "<Gctp v=\"1.0\" env=\"\">" +
                "<System r=\"CprSoeg\">" +
                    "<Service r=\"NAVNE3\">" +
                    "<CprServiceHeader r=\"NAVNE3\" ts=\"20090227095206394457\"/>" +
                    "<CprData u=\"O\">" +
                        "<Rolle r=\"HovedRolle\">" +
                            "<Field r=\"CNVN_TIMSTP\" v=\"20010719120552483903\"/>" +
                            "<Field r=\"CNVN_SYSTEMTIMSTP\" v=\"20010719120552470931\"/>" +
                            "<Field r=\"CNVN_STATUS\" v=\"01\"/><Field r=\"CNVN_STATUSTIMSTP\" v=\"20010317120000000000\"/>" +
                            "<Field r=\"CNVN_STATUSSTARTDATO\"/><Field r=\"CNVN_STATUSDATOUSM\"/>" +
                            "<Field r=\"CNVN_KOEN\" v=\"K\"/>" +
                            "<Field r=\"CNVN_STARTMYNKOD\" v=\"7796\" t=\"Vissenbjerg,Assens\" tl=\"Vissenbjerg Sogn, Assens Kommune\"/>" +
                            "<Field r=\"CNVN_NVNTIMSTP\" v=\"20010719120552483903\"/>" +
                            "<Field r=\"CNVN_FORNVN\" v=\"Katrine\"/>" +
                            "<Field r=\"CNVN_FORNVNMRK\" v=\"&plus;\"/>" +
                            "<Field r=\"CNVN_MELNVN\" v=\"Gangelhoff Damgaard\"/>" +
                            "<Field r=\"CNVN_MELNVNMRK\"/><Field r=\"CNVN_EFTERNVN\" v=\"Knudsen\"/>" +
                            "<Field r=\"CNVN_EFTERNVNMRK\"/>" +
                            "<Field r=\"CNVN_SLAEGTNVN\" v=\"Knudsen\"/><Field r=\"CNVN_SLAEGTNVNMRK\"/>" +
                            "<Field r=\"CNVN_STARTDATO\" v=\"199310241308\"/>" +
                            "<Field r=\"CNVN_STARTDATOUSM\"/>" +
                            "<Field r=\"CNVN_ADRNVNMYNKOD\" v=\"0000\" t=\"Ukendt Myndighed\" tl=\"Ukendt Myndighed\"/>" +
                            "<Field r=\"CNVN_ADRNVNTIMSTP\" v=\"19931024000000000000\"/>" +
                            "<Field r=\"CNVN_ADRNVN\" v=\"Knudsen,Katrine Gangelhoff D\" t=\"Katrine Gangelhoff D Knudsen\"/>" +
                            "<Field r=\"CNVN_INDRAP\" v=\"NEJ\"/>" +
                            "<Field r=\"CNVN_DOKMYNKOD\" v=\"7796\" t=\"Vissenbjerg,Assens\" tl=\"Vissenbjerg Sogn, Assens Kommune\"/>" +
                            "<Field r=\"CNVN_DOKTIMSTP\" v=\"20010719120552483903\"/>" +
                            "<Field r=\"CNVN_DOK\" v=\"JA\"/>" +
                            "<Field r=\"CNVN_MYNTXTMYNKOD\"/>" +
                            "<Field r=\"CNVN_MYNTXTTIMSTP\"/>" +
                            "<Field r=\"CNVN_MYNTXT\"/><Field r=\"HISTORIKMRK\"/>" +
                        "</Rolle>" +
                    "</CprData>" +
                    "<Kvit r=\"Ok\" t=\"\" v=\"0\"/>" +
                    "</Service>" +
                "</System>" +
                "</Gctp>" +
                "</root>\r\n"
    }
    */

}


/*
class CprController {

    def logonOK() {
        String xml = buildLogonResponse('returKode', 'Signon udført', '900')
        response.setHeader('Set-Cookie', 'Token=ZZZabcdefgh')
        render(text: xml, contentType:"text/xml", encoding:"ISO-8859-1")
    }

    def logonFailed() {
        String xml = buildLogonResponse('returKode', 'Token kendes ikke', '901')
        render(text: xml, contentType:"text/xml", encoding:"ISO-8859-1")
    }

    private String buildLogonResponse(r, t, v) {
        def writer = new StringWriter()
        def builder = new MarkupBuilder(writer)
        builder.mkp.xmlDeclaration(version: "1.0", encoding: "ISO-8859-1", standalone: "yes")
        builder.root(xmlns: "http://www.cpr.dk") {
            Gctp(v: "1.0") {
                Stik {
                    Kvit(r: r, t: t, v: v)
                }
            }
        }
        return writer.toString()
    }

}
*/
