package personservice.service

import grails.plugin.spock.UnitSpec
import personservice.service.gateway.CPRRegisterGateway
import personservice.service.gateway.builders.ServiceRequestXMLBuilder

class RequestXMLBuilderSpecification extends UnitSpec {

    void 'test service XML request'() {
        setup:
        def xml
        ServiceRequestXMLBuilder builder = new ServiceRequestXMLBuilder('cpr', 'service')

        when:
        xml = new XmlSlurper().parseText(builder.buildXML())

        then:
        def gctp = xml.depthFirst().find {it.name() == 'Gctp'}
        gctp.name() == 'Gctp'
        gctp.@v == '1.0'
        gctp.System.@r == 'CprSoeg'
        gctp.System.Service.@r == 'service'
        gctp.System.Service.CprServiceHeader.@r == 'service'
        gctp.System.Service.CprServiceHeader.Key.children().size() == 1
        gctp.System.Service.CprServiceHeader.Key.Field.@r == 'PNR'
        gctp.System.Service.CprServiceHeader.Key.Field.@v == 'cpr'

    }

}
