package personservice
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import personservice.domain.Person
import personservice.service.PersonService

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(PersonService)
@Build(Person)
class PersonServiceTests {

    void testSomething() {
        def person = Person.build()
        println person.dump()
        assert person
    }
}
