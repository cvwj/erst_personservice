import grails.util.Holders
import personservice.service.gateway.CPRRegisterGateway

class PersonServiceGrailsPlugin {
    // the plugin version
    def version = "0.1"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.1 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
            "grails-app/views/error.gsp"
    ]

    // TODO Fill in these fields
    def title = "Person Service Plugin" // Headline display name of the plugin
    def author = "cvwj & ps"
    def authorEmail = ""
    def description = '''\
The plugin provides integration with the danish CPR register. It injects a service, personService,
which exposes the API.
This service autoconfigure a datasource for its own domain model.
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/person-service"

    // Extra (optional) plugin metadata

    // License: one of 'APACHE', 'GPL2', 'GPL3'
//    def license = "APACHE"

    // Details of company behind the plugin (if there is one)
//    def organization = [ name: "My Company", url: "http://www.my-company.com/" ]

    // Any additional developers beyond the author specified above.
//    def developers = [ [ name: "Joe Bloggs", email: "joe@bloggs.net" ]]

    // Location of the plugin's issue tracker.
//    def issueManagement = [ system: "JIRA", url: "http://jira.grails.org/browse/GPMYPLUGIN" ]

    // Online location of the plugin's browseable source code.
//    def scm = [ url: "http://svn.codehaus.org/grails-plugins/" ]

    def doWithWebDescriptor = { xml ->
        // TODO Implement additions to web.xml (optional), this event occurs before
    }

    def doWithSpring = {
        // TODO Implement runtime spring config (optional)
        communicator(CPRRegisterGateway.CPRHttpsCommunicator)
        {
            username = application.config.personService.cprGateway.username
            password = application.config.personService.cprGateway.password
            /*
                Endpoint@CPR:
                Produktion prod.ajou.cpr.dk:683
                Demo demo.ajou.cpr.dk:681
                Extern test xtst.ajou.cpr.dk:682
                Login-path: /cics/dmwg/cscwbsgn
                Service-path:
                http://www.cpr.dk/cpr_artikler/Files/Fil6/4157.pdf
             */
            endpoint = application.config.personService.cprGateway.endpoint
        }
        cprServicesGateway(CPRRegisterGateway) {
            cprCommunicator = ref("communicator")
        }

    }


    def doWithApplicationContext = { applicationContext ->
        // Inject the gateway into the personService
        applicationContext.personService.cprRegisterGateway = applicationContext.cprServicesGateway
    }
}
