package personservice.service.gateway

enum CPRRegisterGatewayErrorCode {
    LOGON_FAILURE,
    SERVICE_FAILURE,
    NO_XML_RESPONSE
}
