package personservice.service.gateway.builders

class LogonXMLBuilder extends XMLBuilder {
    String user, password

    LogonXMLBuilder(String user, String password) {
        this.user = user
        this.password = password
    }

    String buildXML() {
        builder.mkp.xmlDeclaration(version: "1.0", encoding: "ISO-8859-1", standalone: "yes")
        builder.root(xmlns: "http://www.cpr.dk") {
            Gctp(v: "1.0") {
                Stik(function: "signon", userid: user, password: password)
            }
        }
        return writer.toString()
    }
}
