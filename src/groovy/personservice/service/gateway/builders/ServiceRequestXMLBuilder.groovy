package personservice.service.gateway.builders

class ServiceRequestXMLBuilder extends XMLBuilder {
    String token, service, cpr

    public ServiceRequestXMLBuilder(String cpr, String service) {
        this.cpr = cpr
        this.service = service
    }

    String buildXML() {
        builder.Gctp(v: "1.0") {
            System(r: "CprSoeg") {
                Service(r: service) {
                    CprServiceHeader(r: service) {
                        Key {
                            Field(r: 'PNR', v: cpr)
                        }
                    }
                }
            }
        }
        return writer.toString()
    }
}