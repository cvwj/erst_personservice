package personservice.service.gateway.builders

/**
 * Updates the password of the user.
 * CPR documentation: http://www.cpr.dk/cpr_artikler/Files/Fil6/4157.pdf
 */
class SkiftPasswordXMLBuilder extends XMLBuilder {
    String user, newPassword

    SkiftPasswordXMLBuilder(String user, String password, String newPassword) {
        this.user = user
        this.password = password
        this.newPassword = newPassword
    }

    String buildXML() {
        builder.mkp.xmlDeclaration(version: "1.0", encoding: "ISO-8859-1", standalone: "yes")
        builder.root(xmlns: "http://www.cpr.dk") {
            Gctp(v: "1.0") {
                Sik(function: "newpass", userid: user, password: password, newpass1: newPassword)
            }
        }
        return writer.toString()
    }
}
