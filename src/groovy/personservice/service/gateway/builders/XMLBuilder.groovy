package personservice.service.gateway.builders

import groovy.xml.MarkupBuilder

abstract class XMLBuilder {
    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)

    abstract String buildXML()
}