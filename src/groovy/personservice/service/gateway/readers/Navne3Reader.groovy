package personservice.service.gateway.readers

import personservice.service.gateway.data.Navne3Data

class Navne3Reader extends CPRResponseDataReader {

    public Navne3Reader(String xml) {
        super(xml)
    }

    @Override
    Navne3Data toData() {
        new Navne3Data(fornavn: readField('CNVN_FORNVN'),
                mellemnavn: readField('CNVN_MELNVN'),
                efternavn: readField('CNVN_EFTERNVN')
        )
    }


}
