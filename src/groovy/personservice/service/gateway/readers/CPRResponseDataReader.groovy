package personservice.service.gateway.readers

import groovy.util.slurpersupport.GPathResult
import personservice.service.gateway.data.CPRData

abstract class CPRResponseDataReader {

    private ServiceResponseXMLReader xmlReader

    protected CPRResponseDataReader(String xmlData) {
        xmlReader = new ServiceResponseXMLReader()
        xmlReader.parseText(xmlData)
    }

    abstract CPRData toData()

    protected String readField(String field) {
        xmlReader.readField(field)
    }

    private class ServiceResponseXMLReader {
        GPathResult xml

        void parseText(String xmlText) {
            xml = new XmlSlurper().parseText(removeInvalidCharacters(xmlText))
        }

        String readField(String fieldName) {
            xml.depthFirst().find {it.name() == 'Field' && it.@r == fieldName}.@v
        }

        private String removeInvalidCharacters(String xml) {
            xml.replaceAll('&plus;', '')
        }
    }
}
