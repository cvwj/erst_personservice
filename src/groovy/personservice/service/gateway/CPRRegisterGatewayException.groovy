package personservice.service.gateway

class CPRRegisterGatewayException extends RuntimeException {
    CPRRegisterGatewayErrorCode errorCode

    public CPRRegisterGatewayException(CPRRegisterGatewayErrorCode errorCode) {
        this.errorCode = errorCode
    }

    public CPRRegisterGatewayException(CPRRegisterGatewayErrorCode errorCode, String message) {
        super(message)
        this.errorCode = errorCode
    }
}
