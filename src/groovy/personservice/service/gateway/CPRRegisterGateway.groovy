package personservice.service.gateway
import personservice.service.gateway.data.*
import personservice.service.gateway.readers.Navne3Reader

class CPRRegisterGateway {
    static String NAVNE3 = 'NAVNE3'
    static String PNRDETA = 'PNR-DETA'
    static String PNRVAERG = 'PNRVAERG'
    static String SOGMARK1 = 'SOGMARK1'
    static String STAM = 'STAM'
    static String STBORG2 = 'STBORG2'
    static String ADRESSE3 = 'ADRESSE3'
    static String ADRESSE4 = 'ADRESSE4'

    private CPRHttpsCommunicator cprCommunicator

    void setCprCommunicator(CPRHttpsCommunicator cprCommunicator) {
        this.cprCommunicator = cprCommunicator
    }

    Navne3Data lookupNavne3(String cpr) throws CPRRegisterGatewayException {
        def response = cprCommunicator.request(cpr, NAVNE3)
        if (response) {
            return new Navne3Reader(response).toData()
        }
        throw new CPRRegisterGatewayException(CPRRegisterGatewayErrorCode.NO_XML_RESPONSE)
    }

    Adresse3Data lookupAdresse3(String cpr) throws CPRRegisterGatewayException {

    }

    Adresse4Data lookupAdresse4(String cpr) throws CPRRegisterGatewayException {

    }

    PnrdetaData lookupPnrdeta(String cpr) throws CPRRegisterGatewayException {

    }

    Sogmark1Data lookupSoegmark1(String cpr) throws CPRRegisterGatewayException {

    }

    StamData lookupStam(String cpr) throws CPRRegisterGatewayException {

    }

    Stborg2Data lookupStborg2Data(String cpr) throws CPRRegisterGatewayException {

    }

    void skiftPassword(String newPassword) throws CPRRegisterGatewayException {
        cprCommunicator.skiftPassword(newPassword)

    }

}
