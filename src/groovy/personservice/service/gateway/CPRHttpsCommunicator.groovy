package personservice.service.gateway

import groovyx.net.http.HTTPBuilder
import personservice.service.gateway.builders.LogonXMLBuilder
import personservice.service.gateway.builders.ServiceRequestXMLBuilder

import static groovyx.net.http.Method.*
import static groovyx.net.http.ContentType.*

class CPRHttpsCommunicator {
    static String KVIT_RETURN_CODE_OK = '900'
    static String KVIT_RETURN_CODE_UNKNOWN_TOKEN = '901'

    private String username
    private String password
    private String endpoint
    private String uripath

    private String token
    private String responseXML

    /** Public API */

    public synchronized String request(String cpr, String service) {
        // TODO
        // this is a critical section
        // for now we need to synchronize, otherwise two different users can mess arround with accesing/setting the token
        // consider to introduce ThreadLocal with (token, user)

        if (!token) {
            logon()
        }

        return handleConnectService(cpr, service)
    }

    /** Private methods */

    private String handleConnectService(String cpr, String service) {
        connectService(cpr, service)
        def kvitServiceReturnCode = readKvitResponseCode()

        if (kvitServiceReturnCode == KVIT_RETURN_CODE_OK) {
            // TODO
            // is there other return code than
            // (900) for the successfull service call ?
            return responseXML

        } else if (kvitServiceReturnCode == KVIT_RETURN_CODE_UNKNOWN_TOKEN) {
            reconnectService(cpr, service)
            return responseXML
        }
        // TODO
        // we need to investigate other return code
        // for now all other return code result in a service call failure
        throw new CPRRegisterGatewayException(CPRRegisterGatewayErrorCode.SERVICE_FAILURE, responseXML)
    }

    private void logon() {
        // TODO
        // this is a critical section
        // for now we need to synchronize, otherwise two different users can mess arround with accesing/setting the token
        // consider to introduce ThreadLocal with (token, user)

        connectLogon()
        def returnCode = readKvitResponseCode()
        if (returnCode != KVIT_RETURN_CODE_OK) {
            throw new CPRRegisterGatewayException(CPRRegisterGatewayErrorCode.LOGON_FAILURE)
        }
    }

    private String readKvitResponseCode() {
        def xml = new XmlSlurper().parseText(responseXML)
        xml.depthFirst().find { node ->
            node.name() == 'Kvit'
        }.@v
    }

    private void connectLogon() {
        responseXML = connect(true, new LogonXMLBuilder(username, password).buildXML())
        println responseXML
    }

    private void connectService(String cpr, String service) {
        responseXML = connect(false, new ServiceRequestXMLBuilder(cpr, service).buildXML())
        println responseXML
    }

    private void reconnectService(String cpr, String service) {
        logon()
        connectService(cpr, service)
        def kvitServiceReturnCode = readKvitResponseCode()
        if (kvitServiceReturnCode != KVIT_RETURN_CODE_OK) {
            throw new CPRRegisterGatewayException(CPRRegisterGatewayErrorCode.SERVICE_FAILURE, responseXML)
        }
    }

    private String connect(boolean logon, String xmlBody) {
        // TODO provide SSL connection

        // send with http builder
        def http = new HTTPBuilder(endpoint + '/')

        http.request(POST, TEXT) {
            uri.path = uripath
            // requestContentType: "text/xml"
            // contentType = "text/xml"
            body = xmlBody

            // TODO - provide headers

            response.success = { resp, reader ->
                if (logon) {
                    token = resp.headers.'Set-Cookie'
                    println 'token: ' + token
                }
                return reader.text
            }
        }
    }
}
